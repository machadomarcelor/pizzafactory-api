<?php

use Illuminate\Database\Seeder;

class TamanhoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tamanho')->insert([
           [
              'nome' => 'Brotinho',
              'qtdSabores' => 1,
              'diametro' => 20,
              'fatias' => 4,
              'valor' => 10.00
           ],
           [
              'nome' => 'Média',
              'qtdSabores' => 2,
              'diametro' => 30,
              'fatias' => 8,
              'valor' => 20.00
           ],
           [
              'nome' => 'Grande',
              'qtdSabores' => 3,
              'diametro' => 40,
              'fatias' => 12,
              'valor' => 30.00
           ],
           [
              'nome' => 'Gigante',
              'qtdSabores' => 4,
              'diametro' => 50,
              'fatias' => 16,
              'valor' => 40.00
           ]
        ]);
    }
}

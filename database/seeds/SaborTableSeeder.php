<?php

use Illuminate\Database\Seeder;

class SaborTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sabor')->insert([
           [
              'nome' => 'Calabresa',
              'descricao' => 'Calabresa, queijo mussarela e orégano'
           ],
           [
              'nome' => 'Mussarela',
              'descricao' => 'Queijo mussarela, orégano, manjericão e azeitonas pretas'
           ],
           [
              'nome' => 'Romana',
              'descricao' => 'Queijo mussarela, tomate, orégano e manjericão'
           ],
           [
              'nome' => 'Bacon',
              'descricao' => 'Bacon, queijo mussarela e orégano'
           ],
           [
              'nome' => 'Lombinho',
              'descricao' => 'Lombo defumado, queijo mussarela e orégano'
           ],
           [
              'nome' => 'Filé',
              'descricao' => 'Iscas de carne, queijo mussarela e batata palha'
           ]
        ]);
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePizzaSaborTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pizza_sabor', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pizza_id')->unsigned()->default(0);
            $table->foreign('pizza_id')->
               references('id')->on('pizza')->
               onDelete('cascade');
            $table->integer('sabor_id')->unsigned()->default(0);
            $table->foreign('sabor_id')->
               references('id')->on('sabor')->
               onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pizza_sabor');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTamanhoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tamanho', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->integer('qtdSabores');
            $table->integer('diametro');
            $table->integer('fatias');
            $table->decimal('valor', 5, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tamanho');
    }
}

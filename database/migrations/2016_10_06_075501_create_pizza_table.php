<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePizzaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pizza', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pedido_id')->unsigned()->default(0);
            $table->foreign('pedido_id')->
               references('id')->on('pedido')->
               onDelete('cascade');
            $table->integer('tamanho_id')->unsigned()->default(0);
            $table->foreign('tamanho_id')->
               references('id')->on('tamanho')->
               onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pizza');
    }
}

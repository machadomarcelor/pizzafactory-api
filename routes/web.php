<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/pizzaria', 'PizzariaController@getPizzaria');

Route::get('/sabores/{id?}', 'SaborController@getSabor')->where('id', '[0-9]+');

Route::get('/precospizzas/{id?}', 'TamanhoController@getTamanho')->where('id', '[0-9]+');

// lista pedidos em geral ou definido pelo id
Route::get('/pedidos/{id?}', 'PedidoController@getPedidos')->where('id', '[0-9]+');

//lista pedidos em aberto ou pelo id do cliente
Route::get('/listadepedidos/{id?}', 'PedidoController@getPedidosCliente')->where('id', '[0-9]+');

Route::get('/cliente/{id?}', 'ClienteController@getCliente')->where('id', '[0-9]+');

Route::get('/pizza/{id?}', 'PizzaController@getPizza')->where('id', '[0-9]+');
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sabor extends Model
{
   protected $table = 'sabor';
   
   protected $hidden = array('created_at', 'updated_at');
   
   public function pizzas()
   {
      return $this->belongsToMany('App\Pizza');
   }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
   protected $table = 'cliente';
   
   protected $hidden = array('created_at', 'updated_at');
   
   public function Pedidos()
   {
      return $this->hasMany('App\Pedido');
   }
}

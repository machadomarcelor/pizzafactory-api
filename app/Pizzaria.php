<?php

namespace App;

class Pizzaria
{
   public static function dados() {
      
      return [
         'Nome' => 'Pizza Factory', 
         'Endereco' => 'Rua Web Service, 80 - IFC, Camboriú - SC', 
         'Telefone' => '3069.0171', 
         'Horario' => 'Tele-entrega das 11h da manhã até 1h da madrugada'
      ];
   }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pizza extends Model
{
   protected $table = 'pizza';
   
   protected $hidden = array('created_at', 'updated_at');
   
   public function Pedido()
   {
      return $this->belongsTo('App\Pedido');
   }
   
   public function Tamanho()
   {
      return $this->belongsTo('App\Tamanho');
   }
   
   public function Sabores()
   {
      return $this->belongsToMany('App\Sabor');
   }
}

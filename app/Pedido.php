<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
   protected $table = 'pedido';
   
   protected $hidden = array('created_at', 'updated_at');
   
   public function Cliente()
   {
      return $this->belongsTo('App\Cliente');
   }
   
   public function Pizzas()
   {
      return $this->hasMany('App\Pizza')->with('Tamanho');
   }
}
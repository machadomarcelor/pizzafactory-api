<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tamanho;
use App\Http\Requests;

class TamanhoController extends Controller
{
    public function getTamanho($id = null){
      $tamanho;
      
      if (isset($id)){
         $tamanho = Tamanho::find($id);
      }
      else{
         $tamanho = Tamanho::all();
      }
      
      return $tamanho->toJson();
   }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use App\Http\Requests;

class ClienteController extends Controller
{
    public function getCliente($id = null){
      $clientes;
      
      if (isset($id)){
         $clientes = Cliente::find($id);
         //$clientes->cliente = Pedido->cliente();
      }
      else{
         $clientes = Cliente::all();
      }
       
      return $clientes->toJson();
   }
}

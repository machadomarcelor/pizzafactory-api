<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pizzaria;
use App\Http\Requests;

class PizzariaController extends Controller
{
    public function getPizzaria(){
       return json_encode(Pizzaria::dados());
    }
}

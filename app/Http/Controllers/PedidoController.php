<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pedido;
use App\Cliente;
use App\Http\Requests;

class PedidoController extends Controller
{
    public function getPedidos($id = null){
      $pedidos;
      
      if (isset($id)){
         $pedidos = Pedido::with('Cliente')->with('Pizzas')->find($id);
      }
      else{
         $pedidos = Pedido::with('Cliente')->with('Pizzas')->get();
      }
       
      return $pedidos->toJson();
   }
   
   public function getPedidosCliente($idCliente = null){
      $pedidos;
      
      if (isset($idCliente)){
         $pedidos = Cliente::with('Pedidos')->find($idCliente);
      }
      else{
         $pedidos = Pedido::with('Cliente')->with('Pizzas')->get()->where('entregue', 0);
      }
       
      return $pedidos->toJson();
   }
}

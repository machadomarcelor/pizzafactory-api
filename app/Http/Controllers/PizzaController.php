<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pizza;
use App\Pedido;
use App\Tamanho;
use App\Sabor;
use App\Http\Requests;

class PizzaController extends Controller
{
    public function getPizza($id = null){
      $pizzas;
      
      if (isset($id)){
         $pizzas = Pizza::with('Pedido')->with('Tamanho')->with('Sabores')->find($id);
      }
      else{
         $pizzas = Pizza::with('Pedido')->with('Tamanho')->with('Sabores')->get();
      }
       
      return $pizzas->toJson();
   }
}

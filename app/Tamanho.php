<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tamanho extends Model
{
   protected $table = 'tamanho';
   
   protected $hidden = array('created_at', 'updated_at');
   
   public function pizzas()
   {
      return $this->hasMany('App\Pizza');
   }
}
